const NYTBaseUrl = "https://api.nytimes.com/svc/topstories/v2/";
const ApiKey = "10fcafbf833f464186f75583a4b36cca"; // replace with NYT API key
const SECTIONS = "home, arts, automobiles, books, business, fashion, food, health, insider, magazine, movies, national, nyregion, obituaries, opinion, politics, realestate, science, sports, sundayreview, technology, theater, tmagazine, travel, upshot, world";

function buildUrl(url) {
  return NYTBaseUrl + url + ".json?api-key=" + ApiKey;
}

Vue.component('news-list', {
  props: ['results'],
  template: `
    <section>
      <div class="row">
        <div class="col-md-3 col-6 mb-3" v-for="post in processedPosts">
          <div class="card">
            <div class="card-title text-center">
            {{ post.title }}
            </div>
            <a :href="post.url" target="_blank">
              <img class="img-fluid card-image" :src="post.image_url">
            </a>
            <div class="card-footer">
              <p>{{ post.abstract }}</p>
            </div>
          </div>
        </div>
      </div>
  </section>
  `,
  computed: {
    processedPosts() {
      let posts = this.results;

      // Add image_url attribute
      posts.map(post => {
        let imgObj = post.multimedia.find(media => media.format === "superJumbo");
        post.image_url = imgObj ? imgObj.url : "http://placehold.it/300x200?text=N/A";
      });

      return posts;
    }
  }
});

const vm = new Vue({
  el: '#app',
  data: {
    results: [],
    sections: SECTIONS.split(', '), // create an array of the sections
    section: 'home', // set default section to 'home'
    loading: true,
    title: ''
  },
  mounted() {
    this.getPosts('home');
  },
  methods: {
    getPosts(section) {
      let url = buildUrl(section);
      axios.get(url).then((response) => {
        this.loading = false;
        this.results = response.data.results;
        let title = this.section !== 'home' ? "Top stories in '" + this.section + "' today" : "Top stories today";
        this.title = title + "(" + response.data.num_results + ")";
      }).catch((error) => {
        console.log(error);
      });
    }
  }
});
